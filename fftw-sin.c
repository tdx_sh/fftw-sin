#include<fftw3.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#define	N	4096
#define	Pi	3.1419526

int main(void)
{
	int i;
	double in[N];
	fftw_complex *out;
	fftw_plan my_plan;

	struct timeval tv1,tv2;

	FILE *fptr = fopen("dft_result.txt", "w+");

	if (fptr == NULL) 
    { 
        printf("Could not open file"); 
        return 0; 
    } 
	
	for(i = 0; i < N; i++)
	{
		in[i] = sin(1000 * (2 * Pi) * i / 44100);
	}
	gettimeofday(&tv1,NULL);
	out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*N);			//allocating memory
	
	my_plan = fftw_plan_dft_r2c_1d(N, in, out, FFTW_ESTIMATE); 		//Here we set which kind of transformation we want to perform

	fftw_execute(my_plan); 								//Execution of FFT

	fftw_destroy_plan(my_plan);							//Destroy plan
	gettimeofday(&tv2,NULL);
	printf("Time: %d\n",1000000 * (tv2.tv_sec - tv1.tv_sec) + (tv2.tv_usec - tv1.tv_usec));

	for(i=0;i<N;i++)
	{
		fprintf(fptr, "%.4f\n", sqrt( (*(out + i)[0]) * (*(out + i)[0]) + (*(out + i)[1]) * (*(out + i)[1]) ));
	}
	
	
	fclose(fptr);
	fftw_free(out);			 						//Free memory
	printf("OK\n");
	return 0;
}
