# fftw-sin

use fftw to calculate FFT

Compile it with SDK from Yocto/OE. Initialise environment first, add lib path and required librares e.g. libfftw3

$CC -o fftw-sin fftw-sin.c -L~/sysroots/armv7at2hf-neon-angstrom-linux-gnueabi/usr/lib/ -lm -lfftw3